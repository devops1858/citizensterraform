### Vault Details - DO NOT CHANGE ###

vault_role_name         = "cfg-secenclave-p-2-uuc-app-automation-role"
vault_address           = "https://vault-enterprise-uat.corp.internal.citizensbank.com"
vault_creds_workspace   = "vault-sysid-08425-p2-uat"

### LABEL MODULE VARS ###
app_name            = "uuc"
app_environment     = "DEV"

mandatory_tags = {
    ApplicationID     = "SYSID-08425"
    applicationname   = "Tessell-Oracle-AWS"
    assignmentgroup   = "EDS-AWS"    
    PlanviewID        = "PV8337"
    Criticality       = "Tier3"
    Requestor         = "mahendra.subbaiah@citizensbank.com"
    Support           = "DL-EnterpriseDataServicesDatabaseSupportOracle"
    CostCenter        = "9999999"
    DataClass         = "Internal"
    BusinessMapping   = "UUC"
 
}

additional_tags = {
     "BackupPlan"      = "NonProd"
     "Patch Group"     = "rehydrate"
     "Schedule"        = "Default"
     "ServerType"      = "Win_AWS_TO2_uuc"   
}

### EC2 MODULE VARS ###
os                      = "windows"
instance_count          = 1
instance_type           = "t3.large"
key_name                = "WinKP_cfg-secenclave-p-2"
vpc_security_group_ids  = ["sg-060d49f3f87a729cb"]
iam_instance_profile    = "cfg-uuc-infrastructure-ec2-us-east-1"
subnet_ids              = ["subnet-0fae567a6be024e30", "subnet-04391115301d1d9e1", "subnet-000d6f31cf91ff0a6"]
backup_plan             = "NonProd"
vpc_id                  = "vpc-052808c5d3bcdd436"
kms_key                 = "arn:aws:kms:us-east-1:428662071608:alias/cfg-secenclave-p-2-mrk-uuc-infrastructure-EBS-nonpci-cmk"
static_ip               = "true"

/*
### FSx Variables ###
fsx_name_ana                     = "uuc-SYSID-08425-cfg-secenclave-p-2-us-east-1"
create_fsx                       = false
kms_key_id                       = "arn:aws:kms:us-east-1:428662071608:alias/cfg-secenclave-p-2-mrk-uuc-infrastructure-FSX-nonpci-cmk"
storage_capacity                 = "100"
fsx_subnet_ids                   = []
throughput_capacity              = "8"
security_group_ids               = []
deployment_type                  = "MULTI_AZ_1"
preferred_subnet_id              = ""
storage_type                     = "SSD"
dns_ips                          = ["10.162.56.15", "10.162.57.11"]
domain_name                      = "corp.internal.citizensbank.com"
file_system_administrators_group = "DPT_FSx_Admin"
organizational_unit_distinguished_name = "OU=FSX,OU=AWS,OU=ServersSecure,OU=!Common,DC=corp,DC=internal,DC=citizensbank,DC=com"
username                         = "SVC_AWS_FSx"
automatic_backup_retention_days  = "7"
*/

region                  = "us-east-1"
