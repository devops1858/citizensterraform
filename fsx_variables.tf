
### FSX VARIABLES ###
variable "create_fsx" {
  type        = bool
  default     = false
  description = "Flag used to create FSx"
}

variable "kms_key_id"{
  type    = string
  default = ""
}

variable "storage_capacity"{
  type    = string
  default = ""
}

variable "throughput_capacity"{
  type    = string
  default = ""
}

variable "security_group_ids" {
  type        = list
  default     = []
}

variable "automatic_backup_retention_days"{
  type    = string
  default = ""
}

variable "deployment_type"{
  type    = string
  default = ""
}

variable "preferred_subnet_id"{
  type    = string
  default = ""
}

variable "storage_type"{
  type    = string
  default = ""
}

variable "dns_ips" {
  type        = list
  default     = []
}

variable "domain_name"{
  type    = string
  default = ""
}

variable "file_system_administrators_group"{
  type    = string
  default = ""
}

variable "organizational_unit_distinguished_name"{
  type    = string
  default = ""
}

variable "password"{
  type    = string
  default = ""
}

variable "username"{
  type    = string
  default = ""
} 

variable "fsx_name_ana" {
  type = string
  default = ""
}

variable "skip_final_backup" {
    type = bool
    default = false
}

variable "fsx_subnet_ids" {
  type = list
  default = []
}